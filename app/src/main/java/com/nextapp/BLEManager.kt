package com.example.nextapp

import android.app.Activity
import android.bluetooth.*
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.SystemClock
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.ContextCompat.startActivity
import com.nextapp.interface_bootloader
import java.io.File
import java.net.URL

class BLEManager (var activity: Activity?, var storagePath: File,var url: String,var token:String,var bootloaderVersion:String): interface_application,interface_bootloader {
    //////////Bootloader
    override var url_bootloader:String=url
    override var token_bootloader:String=token
    override var storagePath_bootloader:File=storagePath
    override var bootloader_GATTCLIENT:BluetoothGatt?=null
    override var bootloader_version:String=bootloaderVersion
    //////////Application
    override var app_GATTCLIENT: BluetoothGatt?=null
    override var url_app:String=url
    override var token_app:String=token
    override var storagePath_app:File=storagePath

    private var TAG = "BLEManager"
    private var bthHandler: Handler? = Handler()
    private var deviceString:String=""
    var bluetoothAdapter: BluetoothAdapter? = null
    var bluetoothLeScanner: BluetoothLeScanner? = null
    var bluetoothManager: BluetoothManager? = null
    var bluetoothDevice:BluetoothDevice?=null
    var scanCounter=0
    var flag_scan=false

    fun isBluetoothEnabled(): Boolean {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        // Bluetooth is not enabled :)
        // Bluetooth is enabled
        return bluetoothAdapter?.isEnabled ?: // Device does not support Bluetooth
        false
    }
    fun startScan(string: String) {
        flag_scan=true
        deviceString=string
        bluetoothAdapter = bluetoothManager?.adapter
        bluetoothLeScanner = bluetoothAdapter!!.bluetoothLeScanner
        if (bluetoothLeScanner == null) {
            Log.e(TAG, "startScan(): bluetoothLeScanner == null")
            return
        }
        var conDevices = bluetoothManager!!.getConnectedDevices(BluetoothProfile.GATT);
        if (conDevices.size > 0) {
            for (condev in conDevices)
                if (condev.address == deviceString) {
                    gattConnect_application(activity!!,condev)
                    gattConnect_bootloader(activity!!,condev)
                    return
                }
        }
        Log.e("Scan",scanCounter.toString())
        bluetoothLeScanner!!.startScan(leScanCallback) //callback -> onScanResult(int callbackType, ScanResult result)
        bthHandler?.postDelayed(Runnable {
            bluetoothLeScanner!!.stopScan(leScanCallback)
            bluetoothLeScanner = null
            flag_scan=false
            scanCounter++
        }, 3000)
    }
    private val leScanCallback = @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    object : ScanCallback() {
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onScanResult(callbackType: Int, result: ScanResult?){
            if (result?.device?.name != null) {
                if (result?.device.toString()==deviceString) {
                    Log.e(TAG,result.device.name)
                    Log.e(TAG,result.device.toString())
                    Log.e(TAG, "onScanResult(): Found BLE Device(${result.rssi})")
                    gattConnect_application(activity!!,result.device)
                    gattConnect_bootloader(activity!!,result!!.device)
                }
            }
        }
        override fun onScanFailed(errorCode: Int) {
            Log.e(TAG, "ErrorCode: $errorCode")
        }
    }
    fun setDevice(device: BluetoothDevice){
        bluetoothDevice=device
    }
}