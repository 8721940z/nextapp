package com.example.nextapp

import android.Manifest
import android.app.PendingIntent
import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import java.util.*
import kotlin.collections.HashMap


class ble_device(dev_name: String, dev_add: String, dev_rssi: Int){
    var _name : String = dev_name
    var _add : String = dev_add
    var _rssi :Int = dev_rssi
}

class BleInfoAdapter(context: Context?, ble: List<ble_device>?) : BaseAdapter(){
    private var cntxt: Context? = context
    private var myInflater: LayoutInflater? =  context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private var bleDevice: List<ble_device> ? = ble

    override fun getCount(): Int {
        return bleDevice!!.size
    }

    override fun getItem(arg0: Int): Any? {
        return bleDevice?.get(arg0)
    }

    override fun getItemId(position: Int): Long {
        return bleDevice?.indexOf(getItem(position))!!.toLong()
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var rtnview : View = myInflater!!.inflate(R.layout.bluetooth_list_menu, parent, false)

        val img_logo = rtnview.findViewById<View>(R.id.listimg) as ImageView
        val lbl_device = rtnview.findViewById<View>(R.id.listdevice) as TextView
        val lbl_mac = rtnview.findViewById<View>(R.id.listmac) as TextView

        lbl_device.text = bleDevice?.get(position)!!._name
        lbl_mac.text = bleDevice?.get(position)!!._add + "  (${bleDevice?.get(position)!!._rssi})"
        if(bleDevice?.get(position)!!._rssi > -50)img_logo.setImageResource(R.drawable.ble_high)
        else if(bleDevice?.get(position)!!._rssi > -80)img_logo.setImageResource(R.drawable.ble_medium)
        else img_logo.setImageResource(R.drawable.ble_low)



        return rtnview
    }
}

class Bluetooth_Scan : AppCompatActivity() {

    lateinit var swipe: SwipeRefreshLayout
    private lateinit var tx_srh: TextView

    //constant========================================
    val SERVICE_UUID_UART = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
    val CHARACTERISTIC_UUID_UART_TX = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E" //TX
    val CHARACTERISTIC_UUID_UART_RX = "6e400002-b5a3-f393-e0a9-e50e24dcca9e" //RX
    val DESCRIPTOR_UUID_ID_TX_UART = "00002902-0000-1000-8000-00805f9b34fb"
    val ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED"

    //parameters======================================
    var FLAG_REFRESHING = false
    var device_connect = false
    private val STATE_DISCONNECTED = 0
    var address_connected = ""

    var Service_UART: BluetoothGattService? = null
    var CHARACTERISTIC_UART_TX: BluetoothGattCharacteristic? = null
    var CHARACTERISTIC_UART_RX: BluetoothGattCharacteristic? = null

    private lateinit var bluetoothManager: BluetoothManager
    private var bluetoothAdapter: BluetoothAdapter?=null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private val bluetoothLeScanner = BluetoothAdapter.getDefaultAdapter().bluetoothLeScanner

    ////////////save list
    private var BLE_DeviceList = HashMap<String, BluetoothDevice>()
    private var BLE_RSSIlist = HashMap<String, Int>()
    private var BLE_showlist = arrayListOf<ble_device>()

    private lateinit var listView: ListView

    private var BleInfoadapter: BleInfoAdapter? = null
    ////////////

    var mgatt: BluetoothGatt? = null
    var bthHandler: Handler? = Handler()
    private var adapter: SimpleAdapter? = null


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bluetooth_scan)
        try {
            bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            bluetoothAdapter = bluetoothManager?.adapter
            address_connected = intent!!.getStringExtra("device").toString()


            if (!bluetoothAdapter?.isEnabled!!) {
                val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(intent, 1)
            }

        } catch (e: java.lang.Exception) {
            Log.e("bleAdapter", e.message!!)
        }

        listView = findViewById(R.id.BTHlist)
        swipe = findViewById(R.id.swiperefresh)

        setList()
        val checkCallPhonePermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            ) Toast.makeText(this, this.getText(R.string.blepermissionwarn1), Toast.LENGTH_LONG)
                .show()

            val builder = AlertDialog.Builder(this)
            builder.setTitle(this.getText(R.string.warn))
            builder.setIcon(R.mipmap.ic_launcher_round)
            builder.setMessage(this.getString(R.string.blepermissionwarn1))
            builder.setCancelable(false)
            builder.setPositiveButton("OK") { dialogInterface, i ->
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    1
                )

            }///////do sth when touch OK
            builder.create().show()


        } else {///////permission already get
            if (GPSisOPen(this)) {
                refreshBtList()
            } else {
                val builder = AlertDialog.Builder(this)
                builder.setTitle(this.getText(R.string.warn))
                builder.setIcon(R.mipmap.ic_launcher_round)
                builder.setMessage(this.getString(R.string.blepermissionwarn1))
                builder.setCancelable(false)
                builder.setPositiveButton("OK") { dialogInterface, i ->
                    refreshBtList()
                }///////do sth when touch OK
                builder.create().show()
            }
        }
    }

    fun GPSisOPen(context: Context): Boolean {
        val locationManager = context.getSystemService(LOCATION_SERVICE) as LocationManager

        val gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        val network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        return if (gps || network) {
            true
        } else false
    }

    fun openGPS(context: Context?) {
        val GPSIntent = Intent()
        GPSIntent.setClassName(
            "com.android.settings",
            "com.android.settings.widget.SettingsAppWidgetProvider"
        )
        GPSIntent.addCategory("android.intent.category.ALTERNATIVE")
        GPSIntent.data = Uri.parse("custom:3")
        try {
            PendingIntent.getBroadcast(context, 0, GPSIntent, 0).send()
        } catch (e: PendingIntent.CanceledException) {
            e.printStackTrace()
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setList() {
        Log.e("GATT", bluetoothManager.getConnectedDevices(BluetoothProfile.GATT).toString())
        listView.setOnItemClickListener { parent, view, position, id ->
            val loadingHandler: Handler = Handler()
            val loadingRunnale = Runnable {
                LoadingScreen.displayLoadingWithText(this, this.getString(R.string.waiting), false)
            }
            val clickHandler: Handler = Handler()
            val clickRunnable = Runnable {
                device_connect = false
                val devadd = BLE_showlist[position]._add!!
                Log.i("device choose", devadd)
                var conDevices = bluetoothManager.getConnectedDevices(BluetoothProfile.GATT);
                if (conDevices.size > 0) {
                    for (condev in conDevices) {
                        if (condev.address == devadd) {
                            device_connect = true
                            getIntent().putExtra("device", devadd)
                            setResult(RESULT_OK, getIntent())
                            finish()
                        }
                    }
                    if (address_connected != "" && !device_connect) {
                        disconnect(address_connected)
                    }
                    SystemClock.sleep(1000)

                }
                for (r in BLE_DeviceList) {
                    if (r.key == devadd) {
                        var loop = 0
                        while (loop < 5 && !device_connect) {
                            mgatt = BLE_DeviceList.get(r.key)!!.connectGatt(
                                applicationContext,
                                false,
                                gattCallback
                            )
                            SystemClock.sleep(500)
                            if (bluetoothManager?.getConnectionState(
                                    mgatt?.device,
                                    BluetoothProfile.GATT
                                ) != STATE_DISCONNECTED &&
                                bluetoothManager?.getConnectionState(
                                    mgatt?.device,
                                    BluetoothProfile.GATT
                                ) != BluetoothProfile.STATE_DISCONNECTING
                            ) {
                                val builder = AlertDialog.Builder(this@Bluetooth_Scan)
                                device_connect = true
                                getIntent().putExtra("device", mgatt?.device?.address)
                                setResult(RESULT_OK, getIntent())
                                finish()
                            }
                            loop++
                        }
                    }
                }
                if (!device_connect) {

                    refreshBtList()
                }
            }
            val loadingThread = Thread { loadingHandler!!.post(loadingRunnale) }
            val clickThread = Thread { clickHandler!!.postDelayed(clickRunnable, 500) }
            loadingThread.priority = 1
            clickThread.priority = 10
            loadingThread.start()
            clickThread.start()


        }

        //讓 RecyclerView 的 Adapter 更新畫面
        val listener = object : SwipeRefreshLayout.OnRefreshListener {
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onRefresh() {
                clickUpdate(listView)

                BleInfoadapter?.notifyDataSetChanged()

                swipe.isRefreshing = false
            }
        }
        swipe.setOnRefreshListener(listener)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults[0] == 0) {
                    openGPS(this)
                    refreshBtList()
                }/////permission granted
                else {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle(this.getText(R.string.warn))
                    builder.setIcon(R.mipmap.ic_launcher_round)
                    builder.setMessage(this.getString(R.string.blepermissionfail1))
                    builder.setCancelable(false)
                    builder.setPositiveButton("OK") { dialogInterface, i ->
                        setResult(RESULT_CANCELED, getIntent())
                        finish()
                    }///////do sth when touch OK
                    builder.create().show()
                }
            }
            else -> {

            }
        }
    }

    private val leScanCallback = @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    object : ScanCallback() {
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onScanResult(callbackType: Int, result: ScanResult?) {

            if (result!!.device.name != null) {
                BLE_DeviceList[result.device.toString()] = result.device
                BLE_RSSIlist[result.device.toString()] = result.rssi

            }

        }

        override fun onScanFailed(errorCode: Int) {
            Log.e("Scan Failed", "Error Code: $errorCode")
        }
    }

    private val gattCallback = object : BluetoothGattCallback() {
        override fun onCharacteristicRead(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            super.onCharacteristicRead(gatt, characteristic, status)
        }

        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            gatt.discoverServices()
            if (gatt == null) {
                Log.e("TAG", "mBluetoothGatt not created!");
                return;
            }

            val device: BluetoothDevice = bluetoothAdapter!!.getRemoteDevice(address_connected)

            //String address = device.getAddress();
            Log.e("TAG", "onConnectionStateChange ($address_connected) $newState status: $status");

            try {
                when (newState) {
                    BluetoothProfile.STATE_DISCONNECTED -> {
                        broadcastUpdate(ACTION_GATT_DISCONNECTED, address_connected, status);

                    }
                }
            } catch (e: Exception) {
                e.printStackTrace();
            }

        }

        override fun onDescriptorRead(
            gatt: BluetoothGatt?,
            descriptor: BluetoothGattDescriptor?,
            status: Int
        ) {
            super.onDescriptorRead(gatt, descriptor, status)

        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            Service_UART = gatt!!.getService(UUID.fromString(SERVICE_UUID_UART))
            Log.e("BLE_SERVICE", "onServicesDiscovered()" + gatt.services.toString())
            if (Service_UART == null) Log.e(
                "BLE_service",
                "onServicesDiscovered() Service_UART not found"
            )
            CHARACTERISTIC_UART_TX =
                Service_UART!!.getCharacteristic(UUID.fromString(CHARACTERISTIC_UUID_UART_TX))
            CHARACTERISTIC_UART_RX =
                Service_UART!!.getCharacteristic(UUID.fromString(CHARACTERISTIC_UUID_UART_RX))
            gatt.setCharacteristicNotification(CHARACTERISTIC_UART_TX, true)
            //Enable Notification for UART TX
            val _descriptor =
                CHARACTERISTIC_UART_TX?.getDescriptor(UUID.fromString(DESCRIPTOR_UUID_ID_TX_UART))
            if (_descriptor != null) {
                Log.e(
                    "BLE_SERVICE",
                    "onServicesDiscovered() Write to Descriptor ENABLE_NOTIFICATION_VALUE"
                )
                _descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                gatt.writeDescriptor(_descriptor)
            } else Log.e("BLE_SERVICE", "onServicesDiscovered() descriptor == null")

        }
    }

    private fun broadcastUpdate(action: String, address_connected: String, status: Int) {
        val intent = Intent(action)
        sendBroadcast(intent)
    }

    fun BluetoothDevice.removeBond() {
        try {
            javaClass.getMethod("refresh").invoke(this)
        } catch (e: Exception) {
            Log.e("", "Removing bond has been failed. ${e.message}")
        }
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun refreshBtList() {
        FLAG_REFRESHING = true
        BLE_DeviceList.clear()
        BLE_RSSIlist.clear()
        BLE_showlist.clear()
        val conDevices = bluetoothManager.getConnectedDevices(BluetoothProfile.GATT);
        bluetoothLeScanner!!.startScan(leScanCallback)
        LoadingScreen.displayLoadingWithText(this, this.getString(R.string.sreaching), false)
        bthHandler?.postDelayed(Runnable {
            bluetoothLeScanner.stopScan(leScanCallback)

            if (conDevices.size > 0) {

                for (CD in conDevices) {
                    //val hashMap: HashMap<String, String> = HashMap()
                    // hashMap.put("name", CD.name + "(connected)")
                    // hashMap.put("misc", CD.address)
                    BLE_showlist.add(ble_device(CD.name + "(connected)", CD.address, 0))

                }

            }

            for (r in BLE_DeviceList) {

                val rssi = BLE_RSSIlist[r.key] ?: -90
                BLE_showlist.add(ble_device(r.value.name, r.key, rssi))
                //Log.i("BLE_DeviceList-update", hashMap.toString())

            }


            BleInfoadapter = BleInfoAdapter(this, BLE_showlist)



            listView.setAdapter(BleInfoadapter);

            FLAG_REFRESHING = false
            LoadingScreen.hideLoading()
        }, 4000)

    }


    fun disconnect(address: String?) {
        if (bluetoothAdapter == null) {
            Log.w("TAG", "disconnect: BluetoothAdapter not initialized")
            return
        }
        val device: BluetoothDevice = bluetoothAdapter!!.getRemoteDevice(address)
        val connectionState: Int = bluetoothManager.getConnectionState(
            device,
            BluetoothProfile.GATT
        )
        if (mgatt != null) {
            Log.i("TAG", "disconnect")
            if (connectionState != BluetoothProfile.STATE_DISCONNECTED) {
                mgatt?.disconnect()
            } else {
                Log.w(
                    "TAG",
                    "Attempt to disconnect in state: $connectionState"
                )
            }
        }
    }


    fun clickDisconnect(view: View) {
        try {

            disconnect(address_connected)
        } catch (e: Exception) {

        }

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun clickUpdate(view: View) {
        if (!FLAG_REFRESHING) {
            if (!bluetoothAdapter!!.isEnabled) {
                val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(intent, 1)
            }
            refreshBtList()
        } else {
            Toast.makeText(this, this.getString(R.string.refreshtext), Toast.LENGTH_SHORT).show()
        }
    }

    fun clickback(view: View) {
        Log.e(
            "# of the device",
            bluetoothManager.getConnectedDevices(BluetoothProfile.GATT).size.toString()
        )

        if (device_connect) {
            setResult(RESULT_OK, getIntent())
            device_connect = true
            getIntent().putExtra("device", mgatt?.device?.address)
        } else {
            getIntent().putExtra(
                "device",
                bluetoothManager.getConnectedDevices(BluetoothProfile.GATT).size > 0
            )
            setResult(RESULT_CANCELED, getIntent())

            if (bluetoothManager.getConnectedDevices(BluetoothProfile.GATT).size == 0) Toast.makeText(
                this,
                this@Bluetooth_Scan.getText(R.string.blealert1),
                Toast.LENGTH_SHORT
            ).show()
        }

        finish()
    }
}
