package com.example.nextapp

import android.app.Activity
import android.app.AlertDialog
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.Button
import com.nextapp.gatt_bootloader
import com.nextapp.permission
import java.io.File
import java.util.*

class MainActivity : AppCompatActivity(),fileReadWrite,permission{
    private lateinit var bt_test:Button
    override val CHECKPERMISSION: Int=9
    lateinit var serverUrl:String
    lateinit var token: String
    lateinit var storagePath : File
    lateinit var status:Status
    lateinit var user:User
    private lateinit var BLEM: BLEManager
    var BLEMRunnable= Runnable {
        Log.e("BLEM","Start")
        BLEM.bluetoothManager= getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        BLEM.startScan(status.getDevice())
    }
    var listenerHandler:Handler= Handler()
    var listenerRunnable : Runnable= object: Runnable {
        override fun run() {
            if(gatt_bootloader.progress_count==100 ){
                bt_test.isEnabled=true
                bt_test.text="Finish"
            }
            else if(gatt_bootloader.progress_count!=0) bt_test.text=gatt_bootloader.progress_count.toString()
            if(!gatt_bootloader.res_Version.isNullOrEmpty()){
                Log.e("listen",gatt_bootloader.res_Version)
                bt_test.isEnabled=true
//                alertUpdate()
            }
            else{
                if(!gatt_application.firmwareVersion.isNullOrEmpty()){
                    status.setFWVersion(gatt_application.firmwareVersion)
                    BLEM.ifUpgrade(gatt_application.firmwareVersion)
                    BLEM.gattDisconnect_application()

                }
                else if (gatt_application.isConnect){
                    Log.e("MainThread","isConnect")
                    BLEM.read_application()
                }
                else if (!BLEM.flag_scan && BLEM.scanCounter>0){
                    Log.e("scanCounter",BLEM.scanCounter.toString())
                    if(BLEM.scanCounter<5) {
                        BLEM.scanCounter = 0
                        status.setDevice("")
                        runStatus_TEST(status)
                    }
                }
                listenerHandler.postDelayed(this,1000)
            }
            Log.e("MainThread","isConnect ${gatt_application.isConnect}")

        }
    }
    ////ActivityForResult
    val ResultForScanDevice=10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        storagePermission(this)
        bt_test=findViewById(R.id.button)
        bt_test.isEnabled=false
    }

    private fun initial(){
        storagePath=this.getExternalFilesDir(null)!!
        create_saving_directory(storagePath)
        serverUrl=getString(R.string.serverurl)
        token=getString(R.string.Token)
        status= Status(storagePath)
        BLEM= BLEManager(this,storagePath,serverUrl,token,"demo1.3")
        user=User(storagePath,serverUrl,token)
        listenerRunnable.run()

    }

    override fun onRequestPermissionsResult(requestCode: Int,permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            CHECKPERMISSION -> {
                //status.readStatus(storagePath)
                initial()
                runStatus_TEST(status)
            }
            else -> {
            }
        }
    }

    private fun runStatus_TEST(status: Status){
        Log.e("checkStatus",status.checkStatus().toString())
        when(status.checkStatus()){
            status.INITIAL->{
                Log.e("CheckStatus","INITIAL")
                status.setInitial()
                runStatus_TEST(status)
            }
            status.NO_USER->{
                Log.e("CheckStatus","NO_USER")
                user.register()//
                status.setUser(user)
                runStatus_TEST(status)
            }
            status.NO_DEVICE-> {
                Log.e("CheckStatus","NO_DEVICE")
                scan_BLE()
            }
            status.NO_LANGUAGE->{
                Log.e("CheckStatus","NO_LANGUAGE")
                status.setLanguage("eng")
                runStatus_TEST(status)
            }
            status.READY->{
                Log.e("CheckStatus","READY")
                status.setAppVersion()
                Log.e("Device",status.getDevice())
                BLEMRunnable.run()
            }
        }
    }

    private fun scan_BLE(){
        val intent=Intent(this,Bluetooth_Scan::class.java)
        startActivityForResult(intent,ResultForScanDevice)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            ResultForScanDevice->{
                Log.e("Result","ScanBLE")
                if (resultCode== RESULT_OK){
                    LoadingScreen.hideLoading()
                    val device = data!!.getStringExtra("device")
                    status.setDevice(device!!)
                    runStatus_TEST(status)
                }
            }
        }
    }
    private fun alertUpdate(){
        AlertDialog.Builder(this)
            .setTitle("Warning")
            .setMessage("Now Version${gatt_application.firmwareVersion},Latest Version:${gatt_bootloader.res_Version}")
            .setPositiveButton("OK", DialogInterface.OnClickListener { dialogInterface, i ->
                BLEM.downloadFromUrl(gatt_bootloader.res_Version)
                bt_test.isEnabled=false
                bt_test.text="Transport"
            })
            .setNegativeButton("No", DialogInterface.OnClickListener { dialogInterface, i ->
            })
            .setCancelable(false)
            .show()
    }
    fun test(view: View){
        alertUpdate()
    }
}