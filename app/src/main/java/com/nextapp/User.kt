package com.example.nextapp

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.database.DatabaseErrorHandler
import android.util.Log
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONArray
import org.json.JSONObject
import org.mindrot.jbcrypt.BCrypt
import java.io.File
import java.io.IOException
import java.sql.Date

class User(val storagePath:File,val serverUrl: String,val token: String):fileReadWrite{
    private var resStr:String=""
    private var userUrl:String=serverUrl+"users"
    private val personFile="data/personFile.txt"
    ///USER INFORMATION
    private var email=""
    private var password=""
    private var username=""
    private var birthyear=0
    private var height=0
    private var weight=0
    private var drink=0
    private var disease=JSONArray()
    private var license=JSONArray()
    ////
    fun getData(serverUrl:String,email :String,token:String){
        val client = OkHttpClient()
        val request: Request = Request.Builder()
            .url(serverUrl+"/email="+email)
            .header("Authorization",token)
            .get()
            .build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                println("fail : $e")
                Log.e("Error", "connect failed.")
            }
            override fun onResponse(call: Call, response: Response) {
                resStr=response.body?.string().toString()
                Log.e("ResStr",resStr)
            }
        })
    }
    fun postData(){
        val mediaType = "application/json".toMediaType()
        val body =  packetUser().toString().toRequestBody(mediaType)
        val client = OkHttpClient()
        val request: Request = Request.Builder()
            .url(userUrl)
            .header("Authorization",token)
            .post(body)
            .build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                println("fail : $e")
                Log.e("Error", "connect failed.")
            }
            override fun onResponse(call: Call, response: Response) {
                resStr=response.body?.string().toString()
                pack_personalfile(resStr)
            }
        })
    }
    private fun packetUser():JSONObject{
        var jsonObject=JSONObject()
        jsonObject.put("timestamp",java.util.Date().time)
        jsonObject.put("email",email)
        jsonObject.put("company","ryder")
        jsonObject.put("password",password)
        jsonObject.put("username",username)
        jsonObject.put("birthyear",birthyear)
        jsonObject.put("height",height)
        jsonObject.put("weight",weight)
        jsonObject.put("drink",drink)
        jsonObject.put("disease",disease)
        jsonObject.put("license",license)
        return jsonObject
    }
    private fun testSet(){
        email="qa"+java.util.Date().time.toString()+"@test.com"
        password=generateHashedPass("qa")
        username="qa"
        birthyear=1996
        drink=1
        height=170
        weight=65
        disease= JSONArray()
        license= JSONArray()
        disease.put(1).put(0)
        license.put(1).put(0).put(0)
    }
    private fun generateHashedPass(pass: String): String {
        return BCrypt.hashpw(pass, BCrypt.gensalt())
    }
    fun register(){
        testSet()
        val client = OkHttpClient()
        val request: Request = Request.Builder()
            .url(userUrl+"/email="+email)
            .header("Authorization",token)
            .get()
            .build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                println("fail : $e")
                Log.e("Error","connect failed.")
            }

            override fun onResponse(call: Call, response: Response) {
                resStr = response.body?.string().toString()
                Log.e("Response", "${resStr}")
                var temp=JSONObject(resStr)
                if (temp.isNull("data")){
                    postData()
                }
                else ifRegistered(resStr)
            }
        })
    }
    private fun ifRegistered(resStr:String){
        Log.e("This email is existing",resStr)
    }
    private fun pack_personalfile(resStr: String){
        var filestring:String?
        var personObject=JSONObject()
        filestring = read(storagePath,personFile)
        val _id=resStr.substring(1,25)
        personObject.put("_id",_id)
        personObject.put("email",email)
        personObject.put("password",password)
        personObject.put("username",username)
        personObject.put("height",height)
        personObject.put("weight",weight)
        personObject.put("birthyear",birthyear)
        personObject.put("drink",drink)
        personObject.put("disease",disease)
        personObject.put("license",license)
        Log.e("CHECK",personObject.toString())
        try {
            if (filestring.equals("null")) {
                writeLog("[$personObject]",storagePath,personFile)
            }
            else{
                filestring=filestring.removePrefix("[").removeSuffix("]")
                filestring=filestring+","+personObject
                writeLog("[$filestring]",storagePath,personFile)
            }
        } catch (e:IOException) {
            Log.e("Error","$e")
            e.printStackTrace();
        }
    }
    fun getEmail():String{
        return email
    }
}
