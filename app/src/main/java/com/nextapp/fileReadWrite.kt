package com.example.nextapp

import android.util.Log
import org.json.JSONArray
import java.io.File
import java.io.IOException

interface fileReadWrite {
    fun read(storagePath:File,filename : String):String{
        var file = File(storagePath, filename)
        if (!file.exists()) {
            file.createNewFile();
            file.writeText("null")
        }
        return  file.readText(Charsets.UTF_8)
    }
    fun writeLog(input: String,storagePath:File,filename:String){
        var file=File(storagePath, filename)
        try {
            if (!file.exists()) {
                file.createNewFile()
            }
            file.writeText(input)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
    fun create_saving_directory(storagePath:File) {
        var dataDir = File(storagePath, "data")
        if(dataDir.mkdirs()) android.util.Log.i("mkdir", dataDir.toString())
        var binDir = File(storagePath, "bin")
        if(binDir.mkdirs()) android.util.Log.i("mkdir", dataDir.toString())
    }

}