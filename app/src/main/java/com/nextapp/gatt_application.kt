package com.example.nextapp

import android.bluetooth.*
import android.util.Log
import java.util.*

object gatt_application : BluetoothGattCallback(){
    val app_SERIVCE = UUID.fromString("1234E001-FFFF-1234-FFFF-111122223333")
    val CHARACTERISTIC_UUID_UART_READ =     UUID.fromString("1234E005-FFFF-1234-FFFF-111122223333")
    val CHARACTERISTIC_UUID_UART_NOTIFY = UUID.fromString("1234E006-FFFF-1234-FFFF-111122223333")
    val DESCRIPTOR_UUID_UART_Notify  = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")

    var CHARACTERISTIC_UART_NOTIFY : BluetoothGattCharacteristic?= null

    var CHARACTERISTIC_UART_READ : BluetoothGattCharacteristic?= null
    var app_Service_UART: BluetoothGattService? = null
    var firmwareVersion:String=""
    var flag_read=false
    var isConnect=false
    override fun onCharacteristicChanged(
        gatt: BluetoothGatt?,
        characteristic: BluetoothGattCharacteristic?
    ) {
        super.onCharacteristicChanged(gatt, characteristic)
        Log.e("Gatt","CharacteristicChange")
    }
    override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
        gatt.discoverServices()
        Log.e("GATT","ConnectStateChange.")
        if (gatt == null) {
            isConnect=false
            Log.e("TAG", "mBluetoothGatt not created!");
            return;
        }
    }
    override fun onCharacteristicRead(
        gatt: BluetoothGatt?,
        characteristic: BluetoothGattCharacteristic?,
        status: Int
    ) {
        super.onCharacteristicRead(gatt, characteristic, status)
        if (characteristic== CHARACTERISTIC_UART_READ){
            val ByteArray=characteristic!!.value
            for(i in ByteArray) Log.e("Read",i.toString())
            firmwareVersion=ByteArray[1].toString()+"_"+ByteArray[2].toString()+"_"+ByteArray[3].toString()
            flag_read=true
        }
    }
    override fun onCharacteristicWrite(
        gatt: BluetoothGatt?,
        characteristic: BluetoothGattCharacteristic?,
        status: Int
    ) {
        super.onCharacteristicWrite(gatt, characteristic, status)
    }

    override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
        super.onServicesDiscovered(gatt, status)
        Log.e("gatt","onServicesDiscovered")
        if (status != BluetoothGatt.GATT_SUCCESS) {
            Log.e("Service", "onServicesDiscovered() status != BluetoothGatt.GATT_SUCCESS")
            return
        }
        app_Service_UART =gatt!!.getService(app_SERIVCE)
        if (app_Service_UART == null){
            Log.e("Mavlink","onServicesDiscovered() TEST_Service_UART not found")
            return
        }
        /////getCharacteristic
        CHARACTERISTIC_UART_READ = app_Service_UART!!.getCharacteristic(CHARACTERISTIC_UUID_UART_READ)
        CHARACTERISTIC_UART_NOTIFY = app_Service_UART!!.getCharacteristic(CHARACTERISTIC_UUID_UART_NOTIFY)
        /////setNotification
        gatt.setCharacteristicNotification(CHARACTERISTIC_UART_NOTIFY,true)
        var _descriptor = CHARACTERISTIC_UART_NOTIFY?.getDescriptor(DESCRIPTOR_UUID_UART_Notify)
        if (_descriptor != null) {
            Log.e("BLE_SERVICE", "onServicesDiscovered() Write to Descriptor ENABLE_NOTIFICATION_VALUE")
            _descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            gatt.writeDescriptor(_descriptor)
        } else Log.e("BLE_SERVICE", "onServicesDiscovered() descriptor == null")
        isConnect=true
    }

}