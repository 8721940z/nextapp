package com.nextapp

import android.bluetooth.*
import android.util.Log
import com.example.nextapp.gatt_application
import java.math.BigInteger
import java.util.*

object gatt_bootloader : BluetoothGattCallback(){
    val bootloader_SERVICE = UUID.fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E")
    val CHARACTERISTIC_UUID_UART_NOTIFY= UUID.fromString("6E400003-B5A3-F393-E0A9-E50E24DCCA9E") //TX
    val CHARACTERISTIC_UUID_UART_WRITE = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e") //RX
    val DESCRIPTOR_UUID_UART_Notify = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")
    var CHARACTERISTIC_UART_NOTIFY : BluetoothGattCharacteristic?= null
    var CHARACTERISTIC_UART_WRITE : BluetoothGattCharacteristic?= null
    var bootloader_Service_UART: BluetoothGattService? = null
    var ack:ByteArray= byteArrayOf()
    var progress_count=0
    var isConnect=false
    val DELAYTIME=3000
    var res_Version=""
    var get_ack_from_ble=false
    val ack_pass_ota_verify_flash_cmd= byteArrayOf(0x56,0x01)
    val ack_pass_ota_verify_cmd= byteArrayOf(0x52,0x0A)
    val ack_pass_ota_start_cmd= byteArrayOf(0x50,0x05)
    val ack_pass_ota_key_cmd= byteArrayOf(0x49,0x03)
    val ack_pass_ota_clear_cmd= byteArrayOf(0x53,0x0C)
    val ack_pass_ota_write_cmd= byteArrayOf(0x51,0x08)
    val ack_pass_ota_burn_cmd= byteArrayOf(0x57,0x01)
    val ack_pass_ota_flash_cmd = byteArrayOf(0x54,0x08)
    override fun onCharacteristicChanged(
        gatt: BluetoothGatt,
        characteristic: BluetoothGattCharacteristic
    ) {
        super.onCharacteristicChanged(gatt, characteristic)
        if (characteristic == CHARACTERISTIC_UART_NOTIFY) {
            ack = characteristic.value
            /*
            key_state = 0x49,
            start_state = 0x50,
            write_state =0x51 ,
            verify_state = 0x52,
            clearrom_state =0x53,
            flash_state =0x54,
            check_version_state=0x55,
            label_state=0x56
            version state = 0x65

            progress_state = 0x70
             */
            get_ack_from_ble=true
            when (ack[0]) {
                0x70.toByte() ->{
                    progress_count = ack[1].toInt()
                    Log.e("progresss_count", progress_count.toString())
                    Log.e("NOTIFY","0x70")
                }
            }
            for(i in ack){
                Log.e("${ack[0]}",i.toString())
            }
            Log.e("end","---------------------------------")
        }
    }
    override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
        gatt.discoverServices()
        Log.e("GATT_BOOTLOADER","ConnectStateChange.")
        if (gatt == null) {
            isConnect =false
            Log.e("TAG", "mBluetoothGatt not created!");
            return;
        }
    }
    override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
        super.onServicesDiscovered(gatt, status)
        if (status != BluetoothGatt.GATT_SUCCESS)return
        bootloader_Service_UART =gatt!!.getService(bootloader_SERVICE)
        if (bootloader_Service_UART == null){
            Log.e("Mavlink","onServicesDiscovered() TEST_Service_UART not found")
            return
        }
        CHARACTERISTIC_UART_NOTIFY = bootloader_Service_UART!!.getCharacteristic(CHARACTERISTIC_UUID_UART_NOTIFY)
        CHARACTERISTIC_UART_WRITE = bootloader_Service_UART!!.getCharacteristic(CHARACTERISTIC_UUID_UART_WRITE)
        gatt!!.setCharacteristicNotification(CHARACTERISTIC_UART_NOTIFY, true)

        //Enable Notification for UART TX
        val _descriptor = CHARACTERISTIC_UART_NOTIFY?.getDescriptor(DESCRIPTOR_UUID_UART_Notify)
        if (_descriptor != null) {
            _descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            //_descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
            gatt!!.writeDescriptor(_descriptor)
        } else Log.i("Discover", "onServicesDiscovered() descriptor == null")
    }

    /*Callback reporting the result of a characteristic read operation.*/
    override fun onCharacteristicRead(
        gatt: BluetoothGatt,
        characteristic: BluetoothGattCharacteristic,
        status: Int
    ) {
        super.onCharacteristicRead(gatt, characteristic, status)
        val value_bytes = characteristic.value

        //read as bytes
        if (value_bytes == null) Log.e("gatt_bootloader", "onServicesDiscovered() Value=null!") else {
            val output = BigInteger(1, value_bytes).toString(16)
            Log.e("gatt_bootloader", "onServicesDiscovered() Value=$output")
        }

        //read as String
        val value_string = characteristic.getStringValue(0)
        if (value_string == null) Log.e("gatt_bootloader", "onServicesDiscovered() Value=null!") else Log.i(
            "gatt_bootloader",
            "onServicesDiscovered() Value=\"$value_string\""
        )
    }
}