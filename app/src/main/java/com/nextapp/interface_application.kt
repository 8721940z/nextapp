package com.example.nextapp

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.content.Context
import android.util.Log
import java.io.File
import java.util.*

interface interface_application {
    var url_app:String
    var token_app:String
    var storagePath_app: File
    var app_GATTCLIENT: BluetoothGatt?
    fun gattConnect_application(context: Context,device: BluetoothDevice?){
        if (app_GATTCLIENT == null) {
            app_GATTCLIENT =device!!.connectGatt(context, false, gatt_application)
        } else {
            Log.e("APP", "connectDevice(): Gatt Client already created -> Stopping")
        }
    }
    fun isGattConnect_application():Boolean{
        if (app_GATTCLIENT!=null)return true
        return false
    }
    fun gattDisconnect_application(){
        if(app_GATTCLIENT!=null)
            app_GATTCLIENT!!.close()
        else Log.e("APP_Gatt_Client","is not existed.")
    }
    fun read_application():Boolean{
        gatt_application.flag_read=false
        if (gatt_application.CHARACTERISTIC_UART_READ!=null && app_GATTCLIENT!=null){
            Log.e("READ","${app_GATTCLIENT.toString()}")
            app_GATTCLIENT!!.readCharacteristic(gatt_application.CHARACTERISTIC_UART_READ)
            return true
        }
        else{
            return false
        }
    }
    fun isConnected_application(): Boolean {
        return app_GATTCLIENT != null
    }

}