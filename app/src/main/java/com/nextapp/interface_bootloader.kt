package com.nextapp

import android.app.Activity
import android.app.AlertDialog
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.content.Context
import android.content.DialogInterface
import android.os.Build
import android.os.Handler
import android.os.SystemClock
import android.util.Log
import androidx.annotation.RequiresApi
import com.example.nextapp.gatt_application
import okhttp3.*
import okhttp3.internal.toHexString
import okio.ByteString.Companion.toByteString
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.nio.ByteBuffer
import java.util.*

interface interface_bootloader {
    var url_bootloader:String
    var token_bootloader:String
    var storagePath_bootloader:File
    var bootloader_GATTCLIENT:BluetoothGatt?
    var bootloader_version:String
    fun gattConnect_bootloader(context: Context, device: BluetoothDevice?){
        if (bootloader_GATTCLIENT == null) {
            bootloader_GATTCLIENT =device!!.connectGatt(context, false, gatt_bootloader)
        } else {
            Log.e("APP", "connectDevice(): Gatt Client already created -> Stopping")
        }
    }
    fun isGattConnect_bootloader():Boolean{
        if (bootloader_GATTCLIENT!=null)return true
        return false
    }
    fun gattDisconnect_bootloader(){
        if(bootloader_GATTCLIENT!=null)
            bootloader_GATTCLIENT!!.close()
        else Log.e("APP_Gatt_Client","is not existed.")
    }
    fun ota_key_cmd() {
        var sha256key = byteArrayOf(
            0x49.toByte(),
            0x10.toByte(),
            0x25.toByte(),
            0x4a.toByte(),
            0x2a.toByte(),
            0x6e.toByte(),
            0xe0.toByte(),
            0xe3.toByte(),
            0xd5.toByte(),
            0x6d.toByte(),
            0x75.toByte(),
            0x95.toByte(),
            0xb8.toByte(),
            0x5d.toByte(),
            0x85.toByte(),
            0xfc.toByte(),
            0xee.toByte(),
            0x7.toByte(),
            0x7b.toByte(),
            0xd5.toByte(),
            0xcc.toByte(),
            0x5f.toByte(),
            0x4.toByte(),
            0xc7.toByte(),
            0x9e.toByte(),
            0x66.toByte(),
            0x5a.toByte(),
            0x49.toByte(),
            0x3.toByte(),
            0x21.toByte(),
            0x14.toByte(),
            0x42.toByte(),
            0x99.toByte(),
            0x83.toByte(),
            0x1c.toByte(),
            0x81.toByte(),
            0x97.toByte(),
            0x45.toByte(),
            0xb2.toByte(),
            0xd8.toByte(),
            0x6c.toByte(),
            0x31.toByte(),
            0xc5.toByte(),
            0x70.toByte(),
            0x62.toByte(),
            0x35.toByte(),
            0xff.toByte(),
            0xb2.toByte(),
            0x78.toByte(),
            0xae.toByte(),
            0xe2.toByte(),
            0x49.toByte(),
            0xd9.toByte(),
            0x93.toByte(),
            0x3c.toByte(),
            0x54.toByte(),
            0xd9.toByte(),
            0x47.toByte(),
            0xd0.toByte(),
            0xe2.toByte(),
            0x16.toByte(),
            0x17.toByte(), 0x12.toByte(), 0x3d.toByte(), 0x8.toByte(), 0xa3.toByte()
        )
        //byte[] data = sha256key;
        UART_Writebytes(sha256key)
    }
    fun ota_start_cmd(addr: Int, len: Int) {
        val pack = ByteArray(10)
        pack[0] = 0x50.toByte()
        pack[1] = 0x08.toByte()
        pack[2] = (len shr 0).toByte()
        pack[3] = (len shr 8).toByte()
        pack[4] = (len shr 16).toByte()
        pack[5] = (len shr 24).toByte()
        pack[6] = (addr shr 0).toByte()
        pack[7] = (addr shr 8).toByte()
        pack[8] = (addr shr 16).toByte()
        pack[9] = (addr shr 24).toByte()
        UART_Writebytes(pack)
    }
    fun ota_write_cmd(pdata: ByteArray){
        val pack = ByteArray(pdata.size + 2)
        pack[0] = 0x51
        pack[1] = (pdata.size and 0xff).toByte()
        for (i in 2 until pack.size) {
            pack[i] = pdata[i - 2]
        }
        UART_Writebytes(pack)
    }
    fun ota_verify_cmd(check: Int) {
        //byte[] data={(byte)0x52,(byte)0x01,(byte)0x43,(byte)0x12, (byte) 0xab, (byte) 0xcd};
        val data = ByteArray(6)
        data[0] = 0x52.toByte()
        data[1] = 0x01.toByte()
        data[2] = (check shr 0).toByte()
        data[3] = (check shr 8).toByte()
        data[4] = (check shr 16).toByte()
        data[5] = (check shr 24).toByte()
        UART_Writebytes(data)
    }
    fun ota_get_version_cmd() {
        val data = ByteArray(2)
        data[0] = 0x55.toByte()
        data[1] = 0x01.toByte()
        UART_Writebytes(data)
    }
    fun ota_clearrom_cmd(start_addr: Int, end_addr: Int){
//        byte[] data={(byte)0x53,(byte)0x08,(byte)0x00,(byte)0x00, (byte) 0x05, (byte) 0x00
//                ,(byte)0x00,(byte)0x00, (byte) 0x07, (byte) 0x00};
        val data = ByteArray(10)
        data[0] = 0x53
        data[1] = 0x08
        data[2] = (start_addr shr 0).toByte()
        data[3] = (start_addr shr 8).toByte()
        data[4] = (start_addr shr 16).toByte()
        data[5] = (start_addr shr 24).toByte()
        data[6] = (end_addr shr 0).toByte()
        data[7] = (end_addr shr 8).toByte()
        data[8] = (end_addr shr 16).toByte()
        data[9] = (end_addr shr 24).toByte()
        UART_Writebytes(data)
    }
    fun ota_flash_cmd() {
        val data = byteArrayOf(
            0x54.toByte(),
            0x01.toByte(), 0x43.toByte(), 0x12.toByte(), 0xab.toByte(), 0xcd.toByte()
        )
        UART_Writebytes(data)
    }

    fun ota_burn(start_addr: Int, length: Int){
//        byte[] data = new byte[10];
//        data[0] = (byte)0x57;
//        data[1] = (byte)0x01;
//        data[2] = (byte)0x00;
        val data = ByteArray(10)
        data[0] = 0x57
        data[1] = 0x08
        data[2] = (start_addr shr 0).toByte()
        data[3] = (start_addr shr 8).toByte()
        data[4] = (start_addr shr 16).toByte()
        data[5] = (start_addr shr 24).toByte()
        data[6] = (length shr 0).toByte()
        data[7] = (length shr 8).toByte()
        data[8] = (length shr 16).toByte()
        data[9] = (length shr 24).toByte()
        UART_Writebytes(data)
    }
    fun byte_sum(vararg bytes: Byte): Byte {
        var total: Byte = 0
        for (b in bytes) {
            total= (total+b).toByte()
        }
        return total
    }
    fun ota_write_page(address: Int, page: ByteArray) {
        var remain_size = page.size
        var count = 0
        val pack_size = 128 //default 128
        while (remain_size > 0) {
            if (remain_size >= pack_size) {
                ota_write_cmd(Arrays.copyOfRange(page, count, count + pack_size)) //cmd2
                count += pack_size
                remain_size -= pack_size
            } else {
                ota_write_cmd(Arrays.copyOfRange(page, count, count + remain_size)) //cmd2
                count += remain_size
                remain_size = 0
            }
        }
        ota_flash_cmd() //cmd3
        val checksum = byte_sum(*page)
        android.util.Log.e("checksum", "checksum = " + java.lang.Byte.toString(checksum))
        ota_verify_cmd(checksum.toInt()) //cmd4
    }
    fun checksum_error(data: ByteArray): Int {
        var sum = 0
        for (i in data.indices) {
            sum += data[i]
        }
        sum = sum and 0xffff
        return sum
    }
    fun ota_upgrade(ota_binary_data:ByteArray) {
        var address = 0x50000
        var remain_size = ota_binary_data.size
        var count = 0
        val page_size = 4096 //default 4096
        while (remain_size > 0 && gatt_bootloader.ack[0]==gatt_bootloader.ack_pass_ota_verify_cmd[0] && gatt_bootloader.ack[0]==gatt_bootloader.ack_pass_ota_verify_cmd[1]) {
            if (remain_size >= page_size) {
                val tmp = Arrays.copyOfRange(ota_binary_data, count, count + page_size)
                ota_write_page(address, tmp)
                address += page_size
                count = count + page_size
                remain_size -= page_size
            } else {
                val tmp = Arrays.copyOfRange(ota_binary_data, count, count + remain_size)
                ota_write_page(address, tmp)
                address += remain_size
                count = count + remain_size
                remain_size = 0
            }
            gatt_bootloader.progress_count = 100 - remain_size * 100 / ota_binary_data.size
            Log.e("PAGE","${gatt_bootloader.progress_count}")
        }
    }
    fun upgrade_cmd(ota_binary_data: ByteArray) {
        try {
            Thread {
                var fail_count = 0
                var address = 0x50000
                var remain_pack = ota_binary_data.size
                var remain_page = ota_binary_data.size
                var pack_count = 0
                var page_count = 0
                val page_size = 4096
                val pack_size = 128
                var packArray:ByteArray?
                var pageArray:ByteArray?
                ota_verify_flash_cmd(ota_binary_data)
                while (fail_count < 100) {
                    if (!gatt_bootloader.get_ack_from_ble) {
                        fail_count++
                    }
                    when (gatt_bootloader.ack[0]) {
                        gatt_bootloader.ack_pass_ota_verify_flash_cmd[0] -> {
                            if(gatt_bootloader.ack_pass_ota_verify_flash_cmd[1]==gatt_bootloader.ack[1])ota_key_cmd()
                            else ota_verify_flash_cmd(ota_binary_data)
                        }
                        gatt_bootloader.ack_pass_ota_key_cmd[0] ->{
                            if(gatt_bootloader.ack_pass_ota_key_cmd[1]==gatt_bootloader.ack[1]) {
                                ota_clearrom_cmd(0x50000, 0x70000)
                            }
                            else ota_key_cmd()
                        }
                        gatt_bootloader.ack_pass_ota_clear_cmd[0]-> {
                            if (gatt_bootloader.ack_pass_ota_clear_cmd[1]==gatt_bootloader.ack[1]){
                                if(remain_page>page_size)ota_start_cmd(address,page_size)
                            }
                            else ota_clearrom_cmd(0x50000,0x70000)
                        }
                        gatt_bootloader.ack_pass_ota_start_cmd[0]->{
                            if(gatt_bootloader.ack[1]==gatt_bootloader.ack_pass_ota_start_cmd[1]){
                                if (remain_pack>pack_size){
                                    packArray=Arrays.copyOfRange(ota_binary_data,pack_count*pack_size,(pack_count+1)*pack_size)
                                    remain_pack-=pack_size
                                    ota_write_cmd(packArray)
                                }else{
                                    packArray=Arrays.copyOfRange(ota_binary_data,pack_count*pack_size,(pack_count)*pack_size+remain_pack)
                                    remain_pack=0
                                    ota_write_cmd(packArray)
                                }

                            }
                        }
                        gatt_bootloader.ack_pass_ota_write_cmd[0]->{
                            if(gatt_bootloader.ack[1]==gatt_bootloader.ack_pass_ota_write_cmd[1]) {
                                pack_count++
                                Log.e("page_count",pack_count.toString())
                                if(remain_pack==0 || (pack_count*pack_size)%page_size==0)ota_flash_cmd()
                                else if (remain_pack>=pack_size){
                                    packArray=Arrays.copyOfRange(ota_binary_data,pack_count*pack_size,(pack_count+1)*pack_size)
                                    remain_pack-=pack_size
                                    Log.e("remain_pack",remain_pack.toString())
                                    Log.e("pack_length",packArray.size.toString())
                                    ota_write_cmd(packArray)
                                }else{
                                    packArray=Arrays.copyOfRange(ota_binary_data,pack_count*pack_size,(pack_count)*pack_size+remain_pack)
                                    remain_pack=0
                                    ota_write_cmd(packArray)
                                }
                            }
                        }
                        gatt_bootloader.ack_pass_ota_flash_cmd[0]->{
                            if (gatt_bootloader.ack[1]==gatt_bootloader.ack_pass_ota_flash_cmd[1]){
                                if(remain_pack==0){
                                    pageArray=Arrays.copyOfRange(ota_binary_data,page_count*page_size,page_count*page_size+remain_page)
                                    remain_page=0
                                    val checksum = byte_sum(*pageArray)
                                    ota_verify_cmd(checksum.toInt())
                                }
                                else if((pack_count*pack_size)%page_size==0){
                                    pageArray=Arrays.copyOfRange(ota_binary_data,page_count*page_size,(page_count+1)*page_size)
                                    page_count++
                                    remain_page-=page_size
                                    val checksum = byte_sum(*pageArray)

                                    Log.e("page_count",page_count.toString())
                                    Log.e("page_len",pageArray.size.toString())
                                    Log.e("checkSum",checksum.toString())
                                    ota_verify_cmd(checksum.toInt())
                                }
                            }
                        }
                        gatt_bootloader.ack_pass_ota_verify_cmd[0]-> {
                            if (gatt_bootloader.ack[1] == gatt_bootloader.ack_pass_ota_verify_cmd[1]) {
                                if (remain_page > 0) {
                                    if (remain_page >= page_size)
                                        ota_start_cmd(address+page_count*page_size, page_size)
                                    else
                                        ota_start_cmd(address+page_count*page_size, remain_page)
                                } else ota_burn(0x6000, ota_binary_data.size)
                            }else{
//                                Log.e("verify_error",gatt_bootloader.ack[1].toString())
                            }
                        }
                        gatt_bootloader.ack_pass_ota_burn_cmd[0] ->{
                            if (gatt_bootloader.ack[1]==gatt_bootloader.ack_pass_ota_burn_cmd[1]){
                                Log.e("OTA","BURNING")
                                Log.e("fail_count",fail_count.toString())
                            }
                        }
                    }
                }
            }.start()
        }
        catch (e:Exception){
            println(e.message)
        }
    }
    private fun UART_Writebytes(data: ByteArray) {
        val startTime = System.nanoTime()
        var interval: Long = 0

        //Long.toString((System.nanoTime()-startTime)/1000000)
        //Timestamp_text.setText("Time duration:" + ((System.nanoTime()-startTime)/1000000)+"ms");
        gatt_bootloader.get_ack_from_ble = false
        UART_Writebyte(gatt_bootloader.CHARACTERISTIC_UART_WRITE, data)
//        for(i in data)Log.e("UART_WRITE", "${i}")
//        Log.e("writeEnd","----------------------------------")
        while (gatt_bootloader.get_ack_from_ble != true && interval < gatt_bootloader.DELAYTIME) {      //timeout 3s
            interval = (System.nanoTime() - startTime) / 1000000
        }
        if (interval >= gatt_bootloader.DELAYTIME) {
            Log.e("OTA Device NACK", "${data[0]}")
        } else {
            // Log.e("OTA","device  ack");
        }
    }
    private fun UART_Writebyte(characteristic: BluetoothGattCharacteristic?, data: ByteArray) {
        try{
            if (data.size > 0) {
                characteristic!!.value = data
                bootloader_GATTCLIENT!!.writeCharacteristic(characteristic)
                Log.e("write",data[0].toString())
            }
        }
        catch (e : Exception)
        {

        }
    }
    fun isConnected_bootloader(): Boolean {
        return bootloader_GATTCLIENT != null
    }
    fun ifUpgrade(version: String){
        var resStr:String
        var urlGet= url_bootloader+"firmware?hv="+bootloader_version+"/"
        Log.e("url_bootloader","${urlGet}")
        val client = OkHttpClient()
        val request: Request = Request.Builder()
            .url(urlGet)
            .header("Authorization",token_bootloader)
            .get()
            .build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                println("fail : $e")
                android.util.Log.e("Error", "connect failed.")
            }
            override fun onResponse(call: Call, response: Response) {
                resStr= response.body?.string().toString()
                android.util.Log.e("resStr", "$resStr")
                val jsonObject= JSONObject(resStr)
                gatt_bootloader.res_Version= JSONArray(jsonObject.getString("data"))[0].toString().removeSuffix(".bin")
            }
        })
    }
    fun downloadFromUrl(version: String){
        val binPath="bin/"+version+".bin"
        var firmware=File(storagePath_bootloader,binPath)
        try {
            if (!firmware.exists()) {
                firmware.createNewFile();
            }
        } catch (e:IOException) {
            e.printStackTrace();
        }
        //var urlGet="http://59.120.189.128:9000/download/firmware?hv="+deviceName+"&fv="+version+".bin"
        var urlGet=url_bootloader+"firmware?hv="+bootloader_version+"/&fv="+version+".bin"

        val client = OkHttpClient()
        val request: Request = Request.Builder()
            .url(urlGet)
            .header("Authorization",token_bootloader)
            .get()
            .build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                println("fail : $e")
                android.util.Log.e("Error", "connect failed.")
            }
            @RequiresApi(Build.VERSION_CODES.M)
            override fun onResponse(call: Call, response: Response) {
                Log.e("ResStr","${response.body?.toString()}")
                firmware.writeBytes(response.body?.source()!!.readByteArray())
                SystemClock.sleep(1000)
                loadData(firmware)
            }
        })
    }
    fun loadData(binFile: File){
        //get correct filename from uri
        var file_name = binFile.name
        android.util.Log.e("sep", file_name)
        val ota_binary_data=binFile.readBytes()
        upgrade_cmd(ota_binary_data)
    }
    fun ota_verify_flash_cmd(bytes: ByteArray){
        var checksum:ULong= 0u
        for (i in 0..bytes.size-1){
            checksum=checksum+bytes[i].toUByte().toUInt()
        }
        Log.e("len",bytes.size.toString())
        var checkSumBuffer=ByteBuffer.allocate(Long.SIZE_BYTES)
        checkSumBuffer.putLong(checksum.toLong())
        Log.e("checkSum 4Byte",checkSumBuffer.array().toByteString().hex())
        var lenBuffer=ByteBuffer.allocate(Int.SIZE_BYTES)
        lenBuffer.putInt(bytes.size)
        Log.e("len 4Byte",lenBuffer.array().toByteString().hex())
        val data = ByteArray(8)
        data[0] = 0x56.toByte()
        data[1] = checkSumBuffer.array()[7]
        data[2] = checkSumBuffer.array()[6]
        data[3] = checkSumBuffer.array()[5]
        data[4] = checkSumBuffer.array()[4]
        data[5] = lenBuffer.array()[3]
        data[6] = lenBuffer.array()[2]
        data[7] = lenBuffer.array()[1]
        UART_Writebytes(data)
    }
}

